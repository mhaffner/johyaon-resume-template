(TeX-add-style-hook
 "template"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("twentysecondcv" "letterpaper")))
   (TeX-run-style-hooks
    "latex2e"
    "twentysecondcv"
    "twentysecondcv10")
   (TeX-add-symbols
    "skills"))
 :latex)

